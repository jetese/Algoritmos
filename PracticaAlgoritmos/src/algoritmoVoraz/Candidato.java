package algoritmoVoraz;

public class Candidato {
	private int indice;
	private int ci;
	private int fi;
	
	public Candidato(int indice, int ci, int fi) {
		super();
		this.indice = indice;
		this.ci = ci;
		this.fi = fi;
	}
	
	public int getIndice() {
		return indice;
	}
	public void setIndice(int indice) {
		this.indice = indice;
	}
	public int getCi() {
		return ci;
	}
	public void setCi(int ci) {
		this.ci = ci;
	}
	public int getFi() {
		return fi;
	}
	public void setFi(int fi) {
		this.fi = fi;
	}
	
	
}
