package algoritmoVoraz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Entrada {
    
    private static List<Candidato> entradaFichero(String archivo) throws FileNotFoundException, IOException {
        String cadena;
        int size = 0;
        int ci = 0;
        int fi = 0;
        FileReader f = new FileReader(archivo);
        BufferedReader b = new BufferedReader(f);
        List <Candidato> candidatos = new LinkedList<Candidato>();
        cadena = b.readLine();
        if (cadena != null) {
        	size = Integer.parseInt(cadena);
        	int contador = 0;
        	while((cadena = b.readLine())!=null) {
        		String []  bar = cadena. split(" ");
        		ci = Integer.parseInt(bar[0]);
        		fi = Integer.parseInt(bar[1]);
        		candidatos.add(new Candidato(contador,ci,fi));
        		contador++;
            }
        	if(contador != size) {
        		System.out.println("El n�mero de actividades en la primera l�nea del fichero no coincide con el n�mero de actividades en el fichero");
        	}
        }
        else {
        	candidatos = null;
        }
        b.close();
        return candidatos;
    }
    
    private static List<Candidato> entradaTeclado (){
    	Scanner entradaEscaner = new Scanner (System.in); //Creaci�n de un objeto Scanner
    	List <Candidato> candidatos = new LinkedList<Candidato>();
        int ini = 0;
        int fin = 0;
    	System.out.println("Introduzca el n�mero de actividades a introducir:");
    	int numActividades = Integer.parseInt(entradaEscaner.nextLine () );
    	for (int i = 0; i<numActividades; i++) {
    		System.out.println("Introduzca el comienzo da la actividad "+i+":");
        	ini = Integer.parseInt(entradaEscaner.nextLine () );
        	System.out.println("Introduzca el final de la actividad "+i+":");
        	fin = Integer.parseInt(entradaEscaner.nextLine () );
        	System.out.println("");
        	
        	candidatos.add(new Candidato(i,ini,fin));
    	}
    	entradaEscaner.close();
    	return candidatos;
    }

    public static List<Candidato> entrada(String url) {
    	List <Candidato> candidatos = new LinkedList<Candidato>();
    	File estaArchivo = new File (url);
        if (estaArchivo.exists()) {
        	try {
				candidatos = entradaFichero(url);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        else{
        	System.out.println("No existe el fichero de entrada");
        	candidatos = entradaTeclado();
        }
        return candidatos;
    }
   
}
