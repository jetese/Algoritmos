package algoritmoVoraz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

public class Salida {
	private static boolean salidaFichero(List<Candidato> solucion, String url) {
		FileWriter fichero = null;
        PrintWriter pw = null;
        File estaArchivo = new File (url);
        if (!estaArchivo.exists()) {
        	System.out.println("No existe el fichero de salida");
        	return false;
        }
        try
        {
        	
            fichero = new FileWriter(url);
            pw = new PrintWriter(fichero);

            for (int i = 0; i < solucion.size(); i++)
                pw.println("Candidato "+i+": "+solucion.get(i).getIndice());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           // Nuevamente aprovechamos el finally para 
           // asegurarnos que se cierra el fichero.
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
              return false;
           }
        }
        return true;
    }
    
	public static void imprimirSolucion(List <Candidato> candidatos) {
		for ( int i = 0; i< candidatos.size(); i++) {
			System.out.println("Candidato "+i+": "+candidatos.get(i).getIndice());
		}
	}

    public static void salida(List <Candidato> solucion, String url) {
    	if(salidaFichero(solucion,url)) {
    		System.out.println("El fichero se escribi� correctamente \n");
    	}
    	else {
    		System.out.println("El fichero no se pudo escribir correctamente \n");
    	}
        imprimirSolucion(solucion);
    }
    
    public static void printTestTimes(Map<Integer, Long> tiempos) {
    	FileWriter fichero = null;
        PrintWriter pw = null;
        try
        {
            fichero = new FileWriter(System.getProperty("user.dir")+System.getProperty("file.separator")+"resources"+System.getProperty("file.separator")+"tiempos.txt");
            pw = new PrintWriter(fichero);

            tiempos = new TreeMap<Integer, Long>(tiempos);
            Set<Integer> keys = tiempos.keySet();
            for (Iterator<Integer> it = keys.iterator(); it.hasNext(); ) {
                Integer iteraciones = it.next();
                pw.println("Iteraciones: " + iteraciones + "; Tiempo: " + tiempos.get(iteraciones));
            }
            

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           // Nuevamente aprovechamos el finally para 
           // asegurarnos que se cierra el fichero.
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
    }
}
