package algoritmoVoraz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import algoritmoVoraz.Entrada;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//String url = System.getProperty("user.dir")+System.getProperty("file.separator")+"resources"+System.getProperty("file.separator")+"actividades.txt";
		String urlentrada ="";
		String urlsalida = "";
		if (args.length ==2) {
			urlentrada = args[0];
			urlsalida = args[1];
		}
		else if(args.length ==1) {
			urlentrada = args[0];
		}
		
		List <Candidato> candidatos = Entrada.entrada(urlentrada);
		long time = System.currentTimeMillis(); 
		candidatos = ordenarCandidatos(candidatos);
		List <Candidato> solucion = algVoraz(candidatos);
		time = System.currentTimeMillis() -time; 
		System.out.println("Tiempo de ejecución: "+time+"ms");		
		Salida.salida(solucion, urlsalida);
		//Si descomentamos esta linea se lanzarán las pruebas
		//iterateTest();
	}
	
	public static List<Candidato> ordenarCandidatos(List<Candidato> candidatos){
		for (int i=0; i<candidatos.size()-1; i++) {
	        for (int j=0 ; j<candidatos.size() - 1-i; j++)
	               if (candidatos.get(j).getFi() > candidatos.get(j+1).getFi()) {
	            	   Candidato temp = candidatos.get(j);
	            	   candidatos.set(j, candidatos.get(j+1));
	            	   candidatos.set(j+1, temp);
	               }
		}
		return candidatos;
	}

	public static List <Candidato> algVoraz (List <Candidato> candidatos) {
		List <Candidato> solucion = new LinkedList<Candidato>();
		solucion.add(candidatos.get(0)) ;
		for (int i = 1; i < candidatos.size(); i++) {
			
			if (solucion.get(solucion.size()-1).getFi() <= candidatos.get(i).getCi()) {
				solucion.add(candidatos.get(i));
			}
		}
		return solucion;
	}
	public static Candidato Seleccionar (List <Candidato> candidatos, int indice) {
		return candidatos.get(indice);
	}
	
	public static List<Candidato> generateInputRandom(int size){
		List<Candidato> candidatos = new ArrayList<>();
		for(int i = 0; i<size; i++) {
			int inicio = ThreadLocalRandom.current().nextInt(0, 24);
			int end =ThreadLocalRandom.current().nextInt(inicio, 24);
			Candidato c = new Candidato(i, inicio, end);
			candidatos.add(c);
		}
		return candidatos;
	}
	
	public static void iterateTest() {
		System.out.println("Comenzada prueba de rendimiento");
		Map<Integer, Long> tiempos = new HashMap<>();
		List<Candidato> candidatos = new ArrayList<>();
		for(int i = 1; i<101; i++) {
			candidatos = generateInputRandom(i*1000);
			long time = System.currentTimeMillis(); 
			candidatos = ordenarCandidatos(candidatos);
			List <Candidato> solucion = algVoraz(candidatos);
			time = System.currentTimeMillis() -time; 
			System.out.println("Iteraciones: "+ i*1000 +"Tiempo de ejecución: "+time+"ms");
			tiempos.put(i*1000, time);
		}
		
		Salida.printTestTimes(tiempos);
		System.out.print("Terminado test de rendimiento");
	}

}
